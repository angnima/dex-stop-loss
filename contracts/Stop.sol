pragma solidity 0.8.10;

import "./support/UniswapRouter.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";

contract LimitComp is Ownable{

    using SafeMath for uint256;

    //address of the Uniswap v2 router
    address private UNISWAP_V2_ROUTER = 0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff;
    address private UNISWAP_V2_FACTORY = 0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32;
    
    //address of WMATIC token.  This is needed because some times it is better to trade through WMATIC.  
    //you might get a better price using WMATIC.  
    //example trading from token A to WMATIC then WMATIC to token B might result in a better price
    address private constant WMATIC = 0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270;

    address operator = address(0);

    IUniswapV2Router public uniswapV2Router;
    IUniswapV2Factory public uniswapV2Factory;

    constructor (address _operator) {
        uniswapV2Router = IUniswapV2Router(UNISWAP_V2_ROUTER);
        uniswapV2Factory = IUniswapV2Factory(UNISWAP_V2_FACTORY);
        operator = _operator;
    }
    
    // Order Struct
    struct Order {
        string order_id;
        address token_in;
        address token_out;
        address pair_address;
        uint256 amountIn;
        address payable ordered_by;
        uint256 amount_filled;
        uint256 out_amt_fill;
        bool filled;
        bool swapping;
        uint256 approval;
    }

    // token requirement status (must have x amount of token x token)
    bool public token_status = false;
    address public token_address;
    uint256 public token_limit_amt;
    
    // Stop Orders Mapping list
    mapping(string => Order) public _orders;

    // Approval of emergency withdraw by order_id and address of participants
    mapping(string => mapping(address => bool)) _approvals;

    event OrderPlaced(string order_id, address indexed _tokenIn, address indexed _tokenOut, address pair_address, uint256 _amountIn, address indexed _to);
    event OrderTriggered(string order_id, uint256[] amounts);
    event OrderCompleted(string order_id);
    event OrderCancelled(string order_id, address initiator);
    event WithdrawlRequested(string order_id, address initiator);
    event WithdrawlCompleted(string order_id);

    modifier orderExists(string calldata order_id) {
        require(_orders[order_id].token_in != address(0), "Order does not exists!");
        require(_orders[order_id].filled != true, "Order has already been filled!");
        _;
    }
    // Only checks if there's order and ignores if its filled or not
    modifier hasOrder(string calldata order_id) {
        require(_orders[order_id].token_in != address(0), "Order does not exists!");
        _;
    }
    modifier canTriggerOrder(string calldata order_id) {
        require((msg.sender == operator) || (msg.sender == _orders[order_id].ordered_by || (msg.sender == owner())), "No authority to execute swap command!");
        _;
    }

    // address indexed from, address indexed to, uint256 value
    function placeStopOrder(string memory order_id, address pair_address, address _tokenIn, address _tokenOut, uint256 _amountIn, address payable _to) external {
        require(IERC20(_tokenIn).balanceOf(msg.sender) >= _amountIn, "Token In Balance Not enough!");
        require(IERC20(_tokenIn).allowance(msg.sender, address(this)) >= _amountIn, "Token In allowance not enough!");
        
        if(token_status) {
            require(IERC20(token_address).balanceOf(msg.sender) >= token_limit_amt, "Not enough balance for token require!");
        }
        

        //first we need to transfer the amount in tokens from the msg.sender to this contract
        //this contract will have the amount of in tokens
        IERC20(_tokenIn).transferFrom(msg.sender, address(this), _amountIn);

        _orders[order_id] = Order(order_id, _tokenIn, _tokenOut, pair_address, _amountIn, _to, 0, 0, false, false, 0);

        emit OrderPlaced(order_id, _tokenIn, _tokenOut, pair_address, _amountIn, _to);
    }

    function changeOperator(address new_operator) onlyOwner external {
        operator = new_operator;
    }
    

    //this swap function is used to trade from one token to another
    function swap(string calldata order_id) orderExists(order_id) canTriggerOrder(order_id) external {
        Order memory order = _orders[order_id];
        require(IERC20(order.token_in).balanceOf(address(this)) >= order.amountIn.sub(order.amount_filled), "Token In Balance Not enough!");
        require(order.swapping == false, "SWAP Error! Is already swapping.");

        uint256 amount_in = order.amountIn.sub(order.amount_filled);
        order.swapping = true;

        IERC20(order.token_in).approve(UNISWAP_V2_ROUTER, amount_in);

        //path is an array of addresses. this path array will have 3 addresses [tokenIn, WMATIC, tokenOut]
        //the if statement below takes into account if token in or token out is WMATIC.  then the path is only 2 addresses
        address[] memory path;
        if (order.token_in == WMATIC || order.token_out == WMATIC) {
            path = new address[](2);
            path[0] = order.token_in;
            path[1] = order.token_out;
        } else {
            path = new address[](3);
            path[0] = order.token_in;
            path[1] = uniswapV2Router.WETH();
            path[2] = order.token_out;
        }

        uint256[] memory amountOutMins = uniswapV2Router.getAmountsOut(amount_in, path);
        uint256 amt_min = amountOutMins[path.length - 1];
        
        //then we will call swapExactTokensForTokens for the deadline we will pass in block.timestamp
        //the deadline is the latest time the trade is valid for
        uint256[] memory amt = uniswapV2Router.swapExactTokensForTokens(amount_in, amt_min, path, order.ordered_by, block.timestamp);
        
        order.amount_filled = order.amount_filled.add(amt[0]);
        order.out_amt_fill = order.amount_filled.add(amt[1]);
        
        emit OrderTriggered(order_id, amt);

        if(order.amount_filled >= order.amountIn) {
            order.filled = true;
            emit OrderCompleted(order_id);
        }
        order.swapping = false;
    }

    function cancelOrder(string calldata order_id) orderExists(order_id) canTriggerOrder(order_id) external {
        require(_orders[order_id].filled == true, "cancelOrder: Request order already filled!");
        Order memory order = _orders[order_id];

        uint256 return_amt = order.amountIn.sub(order.amount_filled);

        if(return_amt > 0) {
            require(IERC20(order.token_in).balanceOf(address(this)) >= return_amt, "Token In Balance Not enough!");
            IERC20(order.token_in).approve(UNISWAP_V2_ROUTER, return_amt);

            order.amount_filled = order.amount_filled.add(return_amt);
            IERC20(order.token_in).transfer(order.ordered_by, return_amt);
            order.filled = true;
            emit OrderCancelled(order_id, msg.sender);
        }else {
            order.filled = true;
        }
    }

    // Request to return the token. Requires 2 approvals (order creator/owner/operator).
    function tokenReturnRequest(string calldata order_id) hasOrder(order_id) canTriggerOrder(order_id) external {
        require(_approvals[order_id][msg.sender] == false, "Already approved!");
        require(_orders[order_id].filled == true, "Request order already filled!");

        Order memory order = _orders[order_id];

        uint256 return_amt = order.amountIn.sub(order.amount_filled);
        require(return_amt > 0, "Request order already filled!");

        order.approval = order.approval.add(1);

        if(return_amt > 0 && order.approval >= 2) {
            require(IERC20(order.token_in).balanceOf(address(this)) >= return_amt, "Token In Balance Not enough!");

            order.amount_filled = order.amount_filled.add(return_amt);
            IERC20(order.token_in).transfer(order.ordered_by, return_amt);
            order.filled = true;
            emit WithdrawlCompleted(order_id);
        }
        emit WithdrawlRequested(order_id, msg.sender);
    }

    function setRequirementDetails(address _address, uint256 _amt) onlyOwner external {
        require(_address != address(0) && _amt > 0, "Basic Rule for address and amount must be met!");

        token_address = _address;
        token_limit_amt = _amt;
    }

    function setTokenRequirement(bool status) onlyOwner external {
        token_status = status;
    }
    
    //this function will return the minimum amount from a swap input the 3 parameters below and it will return the minimum amount out
    //this is needed for the swap function above
    function getAmountOutMin(address _tokenIn, address _tokenOut, uint256 _amountIn) external view returns (uint256) {
        //path is an array of addresses. this path array will have 3 addresses [tokenIn, WMATIC, tokenOut]
        //the if statement below takes into account if token in or token out is WMATIC.  then the path is only 2 addresses
        address[] memory path;
        if (_tokenIn == WMATIC || _tokenOut == WMATIC) {
            path = new address[](2);
            path[0] = _tokenIn;
            path[1] = _tokenOut;
        } else {
            path = new address[](3);
            path[0] = _tokenIn;
            path[1] = WMATIC;
            path[2] = _tokenOut;
        }
        
        uint256[] memory amountOutMins = uniswapV2Router.getAmountsOut(_amountIn, path);
        return amountOutMins[path.length -1];
    }

    function changeRouterVersion(address _router) onlyOwner public {
        IUniswapV2Router _uniswapV2Router = IUniswapV2Router(_router);
        // Set the router of the contract variables
        uniswapV2Router = _uniswapV2Router;
    }

    function changeFactoryVersion(address _factory) onlyOwner public {
        IUniswapV2Factory _uniswapV2Factory = IUniswapV2Factory(_factory);
        // Set the factory of the contract variables
        uniswapV2Factory = _uniswapV2Factory;
    }

}